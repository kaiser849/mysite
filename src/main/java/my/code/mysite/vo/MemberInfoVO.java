package my.code.mysite.vo;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class MemberInfoVO {

   private String userId;
   private String userPw;
   private String userName;

}
