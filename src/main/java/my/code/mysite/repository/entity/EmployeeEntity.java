package my.code.mysite.repository.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@ToString(exclude = {"menuList"})
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "employeeId")
@Table(name = "employee_list")
public class EmployeeEntity {

    @Id
    private String employeeId;
    private String employeeName;
    private String department;
    private String section;
    private String team;
    private String part;
    private String email;
    private String imageUrl;
    private String employeeLevel;
    private String approvalSite1;
    private String approvalSite2;
    private String approvalSite3;
    private String regDate;
    private String modDate;


    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MenuListEntity> menuList;
}
