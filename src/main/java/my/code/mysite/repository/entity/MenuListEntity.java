package my.code.mysite.repository.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "menu_list")
public class MenuListEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer menuId;
    private String employeeId;

    @ManyToOne
    @JoinColumn(name = "employeeId", updatable = false, insertable = false)
    @NotFound(action = NotFoundAction.IGNORE) // 값이 발견되지 않으면 무시
    private EmployeeEntity employee;
    @OneToOne
    @JoinColumn(name = "menuId", updatable = false, insertable = false)
    private MenuInfoEntity menuInfo;

    public void setIfApi(EmployeeEntity employeeEntity) {
        if (employeeEntity != null) {
            employeeEntity.getMenuList().remove(this);
        }
        employeeId = employeeEntity.getEmployeeId();
        employee = employeeEntity;
        employee.getMenuList().add(this);
    }


}
