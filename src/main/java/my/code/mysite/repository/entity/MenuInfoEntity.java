package my.code.mysite.repository.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "menu_info")
public class MenuInfoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    private Integer sort;
    private String site;
    @Column(name = "menu_name")
    private String menuName;
    @Column(name = "use_frag")
    private String useFrag;
    @Column(name = "del_frag")
    private String delFrag;
}
