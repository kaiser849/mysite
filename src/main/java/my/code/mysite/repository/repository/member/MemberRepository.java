package my.code.mysite.repository.repository.member;

import my.code.mysite.repository.entity.MemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<MemberEntity, String>, MemberRepositoryCustm {
    MemberEntity findByUserId(String userId);

}
