package my.code.mysite.repository.repository.member;

import com.querydsl.core.QueryResults;
import lombok.extern.slf4j.Slf4j;
import my.code.mysite.repository.entity.MemberEntity;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

import static my.code.mysite.repository.entity.QMemberEntity.memberEntity;

@Slf4j
public class MemberRepositoryImpl extends QuerydslRepositorySupport implements MemberRepositoryCustm {


    public MemberRepositoryImpl() {
        super(MemberEntity.class);
    }

    @Override
    public List<MemberEntity> getList() {

        QueryResults<MemberEntity> result =
                from(memberEntity).fetchResults();

        return result.getResults();
    }
}
