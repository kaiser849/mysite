package my.code.mysite.repository.repository.employee;

import my.code.mysite.dashboard.vo.SearchEmployeeVO;
import my.code.mysite.repository.entity.EmployeeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeRepositoryCustm {

    Page<EmployeeEntity> getEmployeeList(Pageable pageable, SearchEmployeeVO searchVO);
}
