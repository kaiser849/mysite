package my.code.mysite.repository.repository.employee;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Predicate;
import my.code.mysite.dashboard.vo.SearchEmployeeVO;
import my.code.mysite.repository.entity.EmployeeEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.transaction.annotation.Transactional;

import static my.code.mysite.repository.entity.QEmployeeEntity.employeeEntity;

public class EmployeeRepositoryCustmImpl extends QuerydslRepositorySupport implements EmployeeRepositoryCustm {

    public EmployeeRepositoryCustmImpl() {
        super(EmployeeEntity.class);
    }

    @Override
    @Transactional
    public Page<EmployeeEntity> getEmployeeList(Pageable pageable, SearchEmployeeVO searchVO) {
        QueryResults<EmployeeEntity> queryResults =
                from(employeeEntity)
                        .where(
                                containsSearchText(searchVO)
                        )
                        .limit(pageable.getPageSize())
                        .offset(pageable.getOffset()).fetchResults();


        return new PageImpl<>(queryResults.getResults(), pageable, queryResults.getTotal());
    }


    private Predicate containsSearchText(SearchEmployeeVO searchVO) {
        if (StringUtils.isEmpty(searchVO.getSearchText())) {
            return null;
        }
        return employeeEntity.employeeName.contains(searchVO.getSearchText());
    }
}
