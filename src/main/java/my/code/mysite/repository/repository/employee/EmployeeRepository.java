package my.code.mysite.repository.repository.employee;

import my.code.mysite.repository.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long>, EmployeeRepositoryCustm {

}
