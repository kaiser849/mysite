package my.code.mysite.service;

import lombok.AllArgsConstructor;
import my.code.mysite.mapper.MemberMapper;
import my.code.mysite.repository.entity.MemberEntity;
import my.code.mysite.repository.repository.member.MemberRepository;
import my.code.mysite.utils.ConverterUtil;
import my.code.mysite.vo.MemberInfoVO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;
    private final MemberMapper mapper;


    public MemberInfoVO getMember(String userId) throws Exception {
        var member = memberRepository.findByUserId(userId);
        MemberInfoVO memberInfo = ConverterUtil.map(member, MemberInfoVO.class);
        return memberInfo;
    }

    public List<MemberInfoVO> getMemberList() throws Exception {
        List<MemberEntity> results = memberRepository.getList();

        List<MemberInfoVO> list =
                results.stream().map(p -> ConverterUtil.map(p, MemberInfoVO.class)).collect(Collectors.toList());


        return list;
    }


    public List<MemberInfoVO> getMemberListMybatis() throws Exception {
        List<MemberInfoVO> results = mapper.getMemberList();
        return results;
    }

}
