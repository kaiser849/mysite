package my.code.mysite.dashboard.vo;


import lombok.*;
import my.code.mysite.repository.entity.EmployeeEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class EmployeeVO {

    @Getter
    @Setter
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor
    @Builder
    public static class Result {

        private String employeeId;
        private String employeeName;
        private String department;
        private String section;
        private String team;
        private String part;
        private String email;
        private String imageUrl;
        private String employeeLevel;
        private String approvalSite1;
        private String approvalSite2;
        private String approvalSite3;
        private String regDate;
        private String modDate;
        private List<MenuListVO.Result> menuList;
        private String siteMenu1;
        private String siteMenu2;
        private String siteMenu3;

        public static EmployeeVO.Result of(EmployeeEntity employee) {
            EmployeeVO.Result result = new EmployeeVO.Result();
            result.setEmployeeId(employee.getEmployeeId());
            result.setEmployeeName(employee.getEmployeeName());
            result.setEmail(employee.getEmail());
            result.setDepartment(employee.getDepartment());
            result.setSection(employee.getSection());
            result.setTeam(employee.getTeam());
            result.setPart(employee.getPart());
            result.setImageUrl(employee.getImageUrl());
            result.setEmployeeLevel(employee.getEmployeeLevel());
            result.setApprovalSite1(employee.getApprovalSite1());
            result.setApprovalSite2(employee.getApprovalSite2());
            result.setApprovalSite3(employee.getApprovalSite3());
            result.setRegDate(employee.getRegDate());
            result.setModDate(employee.getModDate());

            List<MenuListVO.Result> list = new ArrayList<>();
            if (employee.getMenuList() != null && employee.getMenuList().size() > 0) {
                list = employee.getMenuList().stream()
                        .map(MenuListVO.Result::of).collect(Collectors.toList());
            }
            result.setMenuList(list);

            return result;
        }
    }

    @Getter
    @AllArgsConstructor
    public static class PageResponse {
        private final List<EmployeeVO.Result> content;
        private final long total;
    }
}
