package my.code.mysite.dashboard.vo;

import lombok.*;
import my.code.mysite.repository.entity.MenuListEntity;

public class MenuListVO {

    @Getter
    @Setter
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor
    @Builder
    public static class Result {

        private Integer id;
        private Integer menuId;
        private String employeeId;
        private String site;
        private String menuName;


        public static MenuListVO.Result of(MenuListEntity menuListEntity) {
            MenuListVO.Result result = new MenuListVO.Result();

            result.setId(menuListEntity.getId());
            result.setMenuId(menuListEntity.getMenuId());
            result.setEmployeeId(menuListEntity.getEmployeeId());
            result.setSite(menuListEntity.getMenuInfo().getSite());
            result.setMenuName(menuListEntity.getMenuInfo().getMenuName());

            return result;
        }

    }

}
