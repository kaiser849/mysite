package my.code.mysite.dashboard;


import lombok.AllArgsConstructor;
import my.code.mysite.dashboard.service.EmployeeService;
import my.code.mysite.dashboard.vo.EmployeeVO;
import my.code.mysite.dashboard.vo.SearchEmployeeVO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class DashBoardApiController {

    private final EmployeeService service;

    @RequestMapping("/dashboard/list")
    public EmployeeVO.PageResponse getUserList(@PageableDefault(sort = "regDate", direction = Sort.Direction.DESC) Pageable pageable,
                                               SearchEmployeeVO searchVO) {
        return service.getEmployeeList(pageable, searchVO);


    }
}
