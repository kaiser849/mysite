package my.code.mysite.dashboard;


import lombok.AllArgsConstructor;
import my.code.mysite.dashboard.service.EmployeeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@AllArgsConstructor
@RequestMapping("/page")
public class DashBoardController {

    private final EmployeeService service;

    @RequestMapping("/dashboard")
    public String getUserList(Model model) {
        model.addAttribute("empName", "장하영");
        model.addAttribute("imgUrl", "https://groupware.jasongroup.co.kr/idpicture/ja20190527000509_20190528_169313.jpg");
        return "dashboard/user";


    }
}
