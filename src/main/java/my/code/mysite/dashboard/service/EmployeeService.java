package my.code.mysite.dashboard.service;


import lombok.AllArgsConstructor;
import my.code.mysite.dashboard.vo.EmployeeVO;
import my.code.mysite.dashboard.vo.SearchEmployeeVO;
import my.code.mysite.repository.entity.EmployeeEntity;
import my.code.mysite.repository.repository.employee.EmployeeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Transactional(readOnly = true)
    public EmployeeVO.PageResponse getEmployeeList(Pageable pageable, SearchEmployeeVO searchVO) {

        Page<EmployeeEntity> employeeList = employeeRepository.getEmployeeList(pageable, searchVO);
        List<EmployeeVO.Result> results = employeeList.stream()
                .map(EmployeeVO.Result::of)
                .collect(Collectors.toList());

        return new EmployeeVO.PageResponse(results, employeeList.getTotalElements());
    }
}
