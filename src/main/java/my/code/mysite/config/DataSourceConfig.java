package my.code.mysite.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;

import javax.sql.DataSource;

@Configuration
@ConfigurationProperties(prefix = "spring.datasource.hikari")
public class DataSourceConfig extends HikariConfig {

    /**
     * LazyConnectionDataSourceProxy 설정, DB Connection 점유를 Transaction 시작 시점이 아닌 필요한 시점에 점유 한다.
     * https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/jdbc/datasource/LazyConnectionDataSourceProxy.html
     *
     * @return
     */
    @Primary
    @Bean
    public DataSource dataSource() {
        HikariDataSource hikariDataSource = new HikariDataSource(this);
        return new LazyConnectionDataSourceProxy(hikariDataSource);
    }
}
