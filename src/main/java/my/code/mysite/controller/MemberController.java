package my.code.mysite.controller;

import my.code.mysite.service.MemberService;
import my.code.mysite.vo.MemberInfoVO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class MemberController {

    private final MemberService memberService;

    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<MemberInfoVO> getMember(@PathVariable String id) {

        MemberInfoVO member = null;
        try {
            member = memberService.getMember(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity(member, HttpStatus.OK);
    }


    @GetMapping("/users")
    public ResponseEntity<List<MemberInfoVO>> getMemberList() {

        List<MemberInfoVO> member = null;
        try {
            member = memberService.getMemberList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity(member, HttpStatus.OK);
    }


    @GetMapping("/mybatis/users")
    public ResponseEntity<List<MemberInfoVO>> getMemberListMybatis() {

        List<MemberInfoVO> member = null;
        try {
            member = memberService.getMemberListMybatis();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity(member, HttpStatus.OK);
    }

}
