package my.code.mysite.mapper;


import my.code.mysite.vo.MemberInfoVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MemberMapper {

    List<MemberInfoVO> getMemberList();
}
