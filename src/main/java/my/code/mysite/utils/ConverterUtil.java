package my.code.mysite.utils;

import lombok.experimental.UtilityClass;
import org.modelmapper.ModelMapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 사용 예제
 * //Entity -> Dto
 * ConverterUtil.map(snapshot, ProjectSnapshotDto.Detail.class);
 *
 * //Entity -> Dto collection
 * ConverterUtil.mapAll(list, ProjectSnapshotDto.Detail.class);
 *
 * //Dto -> Entity
 * ConverterUtil.map(dto, ProjectStt.class);
 *
 * //Dto -> Entity collection
 * ConverterUtil.mapAll(list, ProjectStt.class);
 */

@UtilityClass
public class ConverterUtil {
    private final ModelMapper modelMapper =  new ModelMapper();

    public <D, T> D map(final T entity, Class<D> outClass) {
        modelMapper.getConfiguration().setAmbiguityIgnored(false);
        return modelMapper.map(entity, outClass);
    }

    public <D, T> D mapAmbiguityIgnored(final T entity, Class<D> outClass) {
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper.map(entity, outClass);
    }

    public <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outClass) {
        return entityList.stream()
                .map(entity -> map(entity, outClass))
                .collect(Collectors.toList());
    }
}
