package my.code.mysite.utils;

import lombok.experimental.UtilityClass;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 날짜 관련 유틸리티
 */
@UtilityClass
public class DateUtil {

    /**
     * @return 'yyyyMMdd' format string, such as '20111203'.
     */
    public static String toBasicIsoDateString(LocalDate source) {
        return Objects.isNull(source)
                ? null : source.format(DateTimeFormatter.BASIC_ISO_DATE);
    }

    /**
     * @return 'yyyy-MM-dd' format string, such as '2011-12-03'.
     */
    public static String toIsoDateString(LocalDate source) {
        return Objects.isNull(source)
                ? null : source.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    /**
     * @param source 'yyyyMMdd' format string, such as '20111203'.
     */
    public static LocalDate basicIsoDateToLocalDate(String source) {
        return !StringUtils.hasText(source)
                ? null : LocalDate.parse(source, DateTimeFormatter.BASIC_ISO_DATE);
    }

    /**
     * @param source 'yyyy-MM-dd' format string, such as '2011-12-03'.
     */
    public static LocalDate isoLocalDateToLocalDate(String source) {
        return !StringUtils.hasText(source)
                ? null : LocalDate.parse(source, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public static LocalDate min(LocalDate d1, LocalDate d2) {
        return d1.isBefore(d2) ? d1 : d2;
    }

    public static LocalDate max(LocalDate d1, LocalDate d2) {
        return d1.isAfter(d2) ? d1 : d2;
    }

    public static YearMonth min(YearMonth d1, YearMonth d2) {
        return d1.isBefore(d2) ? d1 : d2;
    }

    public static YearMonth max(YearMonth d1, YearMonth d2) {
        return d1.isAfter(d2) ? d1 : d2;
    }

    public static boolean equalOrAfter(LocalTime target, LocalTime compare) {
        return !target.isBefore(compare);
    }

    public static boolean equalOrBefore(LocalTime target, LocalTime compare) {
        return !target.isAfter(compare);
    }

    public static boolean equalOrAfter(LocalDate target, LocalDate compare) {
        return !target.isBefore(compare);
    }

    public static boolean equalOrAfter(LocalDateTime target, LocalDateTime compare) {
        return !target.isBefore(compare);
    }

    public static boolean equalOrBefore(LocalDate target, LocalDate compare) {
        return !target.isAfter(compare);
    }

    public static boolean equalOrBefore(LocalDateTime target, LocalDateTime compare) {
        return !target.isAfter(compare);
    }

    public static boolean equalOrAfter(YearMonth target, YearMonth compare) {
        return !target.isBefore(compare);
    }

    public static boolean equalOrBefore(YearMonth target, YearMonth compare) {
        return !target.isAfter(compare);
    }

    public static boolean isBetween(LocalDate targetDate, LocalDate fromInclusive, LocalDate toInclusive) {
        return (targetDate.equals(fromInclusive) || targetDate.isAfter(fromInclusive))
                && (targetDate.equals(toInclusive) || targetDate.isBefore(toInclusive));
    }

    public static boolean isBetween(LocalTime targetTime, LocalTime fromInclusive, LocalTime toInclusive) {
        return (targetTime.equals(fromInclusive) || targetTime.isAfter(fromInclusive))
                && (targetTime.equals(toInclusive) || targetTime.isBefore(toInclusive));
    }

    public static boolean isBetween(LocalDateTime targetDate, LocalDateTime fromInclusive, LocalDateTime toInclusive) {
        return (targetDate.equals(fromInclusive) || targetDate.isAfter(fromInclusive))
                && (targetDate.equals(toInclusive) || targetDate.isBefore(toInclusive));
    }

    public static long countBetween(LocalDate fromInclusive, LocalDate toInclusive) {
        final LocalDate from = DateUtil.min(fromInclusive, toInclusive);
        final LocalDate to = DateUtil.max(fromInclusive, toInclusive);
        return ChronoUnit.DAYS.between(from, to) + 1;
    }

    public static LocalDateTime toLocalDateTime(LocalDate date, LocalTime time) {
        return LocalDateTime.of(date, time);
    }

    /**
     * @param time time of `HHmmss` format
     */
    public static LocalTime toLocalTime(String time) {
        return LocalTime.parse(time, DateTimeFormatter.ofPattern("HHmmss"));
    }

    public static YearMonth toYearMonth(LocalDate date) {
        return YearMonth.of(date.getYear(), date.getMonth());
    }

    public static List<YearMonth> extractYearMonthBetween(YearMonth fromInclusive, YearMonth toInclusive) {
        final YearMonth from = DateUtil.min(fromInclusive, toInclusive);
        final YearMonth to = DateUtil.max(fromInclusive, toInclusive);
        return Stream
                .iterate(from, month -> month.plusMonths(1))
                .limit(ChronoUnit.MONTHS.between(from, to) + 1)
                .collect(Collectors.toList());
    }

    public static List<LocalDate> extractDateBetween(LocalDate fromInclusive, LocalDate toInclusive, int offsetDays) {
        final LocalDate from = DateUtil.min(fromInclusive, toInclusive);
        final LocalDate to = DateUtil.max(fromInclusive, toInclusive);
        return Stream
                .iterate(from, date -> date.plusDays(offsetDays))
                .limit(ChronoUnit.DAYS.between(from, to) / offsetDays + 1)
                .collect(Collectors.toList());
    }

    public static LocalDate firstDateOfWeek(LocalDate date) {
        return date.minusDays(date.getDayOfWeek().getValue() - 1L);
    }

    public static LocalDate lastDateOfWeek(LocalDate date) {
        return firstDateOfWeek(date).plusDays(6);
    }

    public static LocalDate firstDateOfMonth(LocalDate date) {
        return date.minusDays(date.getDayOfMonth() - 1L);
    }

    public static LocalDate lastDateOfMonth(LocalDate date) {
        return firstDateOfMonth(date).plusMonths(1).minusDays(1);
    }

    public static LocalDate firstDateOfMonth(YearMonth yearMonth) {
        return LocalDate.of(yearMonth.getYear(), yearMonth.getMonth(), 1);
    }

    public static LocalDate lastDateOfMonth(YearMonth yearMonth) {
        return lastDateOfMonth(firstDateOfMonth(yearMonth));
    }
}
